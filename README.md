# Breakpoint issue in the Sailfish OS SDK

This project shows an example of how a breakpoint in the source code of a subproject library is ignored.

The test was performed in Sailfihs OS SDK 1701-1, the application was launched in the emulator.

To reproduce the problem put two breakpoints:

- `sample/src/sample.cpp`, line 11
- `libtest/TestClass.cpp`, line 8

and start debugging.
At the first point the application will stop, but the second will be ignored.
