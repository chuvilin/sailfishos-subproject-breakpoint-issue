TEMPLATE = lib
TARGET = test

include($$PWD/../config.pri)

QT -= gui

SOURCES += \
    TestClass.cpp

HEADERS += \
    TestClass.h
