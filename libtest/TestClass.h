#ifndef TESTCLASS_H
#define TESTCLASS_H

#include <QString>

class TestClass {
public:
    explicit TestClass();
    QString getString();
};

#endif // TESTCLASS_H
