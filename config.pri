LIBS_DIR = $$OUT_PWD/../libs
LIBTEST_INCLUDE_PATH = $$PWD/libtest

CONFIG += c++11

isEqual(TEMPLATE, "lib") {
    target = $$TARGET
    target.path = /usr/share/sample/lib
    INSTALLS += target
    DESTDIR = $$LIBS_DIR
}
