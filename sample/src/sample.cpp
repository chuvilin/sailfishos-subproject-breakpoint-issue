#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif

#include <sailfishapp.h>
#include "../libtest/TestClass.h"


int main(int argc, char *argv[]) {
    TestClass testObject;
    QString string = testObject.getString();
    qDebug() << string;
    return SailfishApp::main(argc, argv);
}
