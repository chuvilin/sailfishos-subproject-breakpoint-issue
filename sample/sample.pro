# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = sample

include($$PWD/../config.pri)

CONFIG += sailfishapp

LIBS += -L$$LIBS_DIR -ltest
corelibs.files = $$OUT_PWD/../libs/*.so
corelibs.path  = /usr/share/$$TARGET/lib
INSTALLS += corelibs

HEADERS +=

SOURCES += \
    src/sample.cpp

OTHER_FILES += qml/sample.qml \
    qml/cover/CoverPage.qml \
    qml/pages/MainPage.qml \
    rpm/sample.changes.in \
    rpm/sample.spec \
    rpm/sample.yaml \
    translations/*.ts \
    sample.desktop

INCLUDEPATH += $$LIBTEST_INCLUDE_PATH

SAILFISHAPP_ICONS = 86x86 108x108 128x128 256x256

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n
TRANSLATIONS += translations/sample-ru.ts
